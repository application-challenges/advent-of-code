package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

var numbersToBeFound = []string{
	"1",
	"2",
	"3",
	"4",
	"5",
	"6",
	"7",
	"8",
	"9",
}

const inputPath = "cmd/day1/input.txt"

// TODO: clean this puzzle
func main() {
	file, err := os.Open(inputPath)
	if err != nil {
		panic(err)
	}

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	numbers := make([]int, 0)
	for scanner.Scan() {
		line := scanner.Text()

		numbersFound := findNumbers(line)

		if len(numbersFound) == 0 {
			continue
		}

		var s [2]string
		if len(numbersFound) == 1 {
			s[0] = numbersFound[0]
			s[1] = numbersFound[0]
		} else {
			s[0] = numbersFound[0]
			s[1] = numbersFound[len(numbersFound)-1]
		}

		n1 := getNum(s[0])
		n2 := getNum(s[1])
		n := getNum(fmt.Sprintf("%d%d", n1, n2))

		numbers = append(numbers, n)
	}

	total := 0
	for _, n := range numbers {
		total += n
	}

	fmt.Println("total", "->", total)
}

func findNumbers(line string) []string {
	found := map[int]string{}

	for _, n := range numbersToBeFound {
		i := strings.Index(line, n)
		if i == -1 {
			continue
		}

		j := strings.LastIndex(line, n)
		if j == -1 {
			continue
		}

		found[i] = n
		found[j] = n
	}

	stringNumbers := make([]string, 0, len(found))
	indexes := make([]int, 0, len(found))
	for k, _ := range found {
		indexes = append(indexes, k)
	}

	sort.Ints(indexes)
	for _, k := range indexes {
		stringNumbers = append(stringNumbers, found[k])
	}

	return stringNumbers
}

func getNum(input string) int {
	n, err := strconv.Atoi(input)
	if err == nil {
		return n
	}

	switch input {
	case "one":
		return 1
	case "two":
		return 2
	case "three":
		return 3
	case "four":
		return 4
	case "five":
		return 5
	case "six":
		return 6
	case "seven":
		return 7
	case "eight":
		return 8
	case "nine":
		return 9
	default:
		panic("unknown number")
	}
}
