package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

const (
	inputPath = "cmd/day2/input.txt"
)

// TODO: clean this puzzle
func main() {
	file, err := os.Open(inputPath)
	if err != nil {
		panic(err)
	}

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	minGameSets := make([]set, 0)
	for scanner.Scan() {
		line := scanner.Text()

		game, err := newGameFromLine(line)
		if err != nil {
			panic(err)
		}

		minimumSet := game.MinimumSetOfCubes()

		minGameSets = append(minGameSets, minimumSet)
	}

	total := 0
	for _, set := range minGameSets {
		total += set.RedCubes * set.BlueCubes * set.GreenCubes
	}

	fmt.Println("total", "->", total)
}

var (
	regexBlue  = regexp.MustCompile("(\\d+)\\s+blue")
	regexGreen = regexp.MustCompile("(\\d+)\\s+green")
	regexRed   = regexp.MustCompile("(\\d+)\\s+red")
)

type set struct {
	BlueCubes  int
	RedCubes   int
	GreenCubes int
}

func newSetFromLine(line string) (set, error) {
	var blueCubes, redCubes, greenCubes int

	sBlue := regexBlue.FindStringSubmatch(line)
	sRed := regexRed.FindStringSubmatch(line)
	sGreen := regexGreen.FindStringSubmatch(line)

	if len(sBlue) == 2 {
		c, err := strconv.Atoi(sBlue[1])
		if err != nil {
			return set{}, errors.New("cannot convert blue cubes")
		}

		blueCubes = c
	}

	if len(sRed) == 2 {
		c, err := strconv.Atoi(sRed[1])
		if err != nil {
			return set{}, errors.New("cannot convert red cubes")
		}

		redCubes = c
	}

	if len(sGreen) == 2 {
		c, err := strconv.Atoi(sGreen[1])
		if err != nil {
			return set{}, errors.New("cannot convert green cubes")
		}

		greenCubes = c
	}

	return set{
		BlueCubes:  blueCubes,
		RedCubes:   redCubes,
		GreenCubes: greenCubes,
	}, nil
}

type game struct {
	ID        int
	drawnSets []set
}

func newGameFromLine(line string) (game, error) {
	record := strings.Split(line, ":")
	if len(record) != 2 {
		return game{}, errors.New("invalid line: invalid record")
	}

	sSets := strings.Split(record[1], ";")

	sets := make([]set, 0, len(sSets))
	for _, s := range sSets {
		set, err := newSetFromLine(s)
		if err != nil {
			return game{}, err
		}

		sets = append(sets, set)
	}

	return game{
		drawnSets: sets,
	}, nil
}

func (g game) MinimumSetOfCubes() set {
	var (
		minBlueCubes  = 0
		minRedCubes   = 0
		minGreenCubes = 0
	)

	for _, drawnSet := range g.drawnSets {
		if drawnSet.BlueCubes > minBlueCubes {
			minBlueCubes = drawnSet.BlueCubes
		}

		if drawnSet.RedCubes > minRedCubes {
			minRedCubes = drawnSet.RedCubes
		}

		if drawnSet.GreenCubes > minGreenCubes {
			minGreenCubes = drawnSet.GreenCubes
		}
	}

	if minBlueCubes < 0 {
		minBlueCubes = 0
	}
	if minGreenCubes < 0 {
		minGreenCubes = 0
	}
	if minRedCubes < 0 {
		minRedCubes = 0
	}

	return set{
		BlueCubes:  minBlueCubes,
		RedCubes:   minRedCubes,
		GreenCubes: minGreenCubes,
	}
}
