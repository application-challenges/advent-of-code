package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

const (
	inputPath = "cmd/day2/input.txt"

	maxRedCubes   = 12
	maxGreenCubes = 13
	maxBlueCubes  = 14
)

// TODO: clean this puzzle
func main() {
	file, err := os.Open(inputPath)
	if err != nil {
		panic(err)
	}

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	gamesIDs := make([]int, 0)
	for scanner.Scan() {
		line := scanner.Text()

		game, err := newGameFromLine(line, maxBlueCubes, maxRedCubes, maxGreenCubes)
		if err != nil {
			panic(err)
		}

		if !game.IsPossible() {
			continue
		}

		gamesIDs = append(gamesIDs, game.ID)
	}

	total := 0
	for _, id := range gamesIDs {
		total += id
	}

	fmt.Println("total", "->", total)
}

var (
	regexBlue  = regexp.MustCompile("(\\d+)\\s+blue")
	regexGreen = regexp.MustCompile("(\\d+)\\s+green")
	regexRed   = regexp.MustCompile("(\\d+)\\s+red")
)

type set struct {
	BlueCubes  int
	RedCubes   int
	GreenCubes int
}

func newSetFromLine(line string) (set, error) {
	var blueCubes, redCubes, greenCubes int

	sBlue := regexBlue.FindStringSubmatch(line)
	sRed := regexRed.FindStringSubmatch(line)
	sGreen := regexGreen.FindStringSubmatch(line)

	if len(sBlue) == 2 {
		c, err := strconv.Atoi(sBlue[1])
		if err != nil {
			return set{}, errors.New("cannot convert blue cubes")
		}

		blueCubes = c
	}

	if len(sRed) == 2 {
		c, err := strconv.Atoi(sRed[1])
		if err != nil {
			return set{}, errors.New("cannot convert red cubes")
		}

		redCubes = c
	}

	if len(sGreen) == 2 {
		c, err := strconv.Atoi(sGreen[1])
		if err != nil {
			return set{}, errors.New("cannot convert green cubes")
		}

		greenCubes = c
	}

	return set{
		BlueCubes:  blueCubes,
		RedCubes:   redCubes,
		GreenCubes: greenCubes,
	}, nil
}

type game struct {
	ID int

	maxRedCubes   int
	maxGreenCubes int
	maxBLueCubes  int

	drawnSets []set
}

func newGameFromLine(line string, maxBlueCubes, maxRedCubes, maxGreenCubes int) (game, error) {
	record := strings.Split(line, ":")
	if len(record) != 2 {
		return game{}, errors.New("invalid line: invalid record")
	}

	sID, ok := strings.CutPrefix(record[0], "Game ")
	if !ok {
		return game{}, errors.New("invalid line: invalid game id")
	}

	id, err := strconv.Atoi(sID)
	if err != nil {
		return game{}, errors.New("invalid line: cannor convert game id")
	}

	sSets := strings.Split(record[1], ";")

	sets := make([]set, 0, len(sSets))
	for _, s := range sSets {
		set, err := newSetFromLine(s)
		if err != nil {
			return game{}, err
		}

		sets = append(sets, set)
	}

	return game{
		ID: id,

		maxRedCubes:   maxRedCubes,
		maxGreenCubes: maxGreenCubes,
		maxBLueCubes:  maxBlueCubes,

		drawnSets: sets,
	}, nil
}

func (g game) IsPossible() bool {
	for _, drawnSet := range g.drawnSets {
		if drawnSet.BlueCubes > g.maxBLueCubes {
			return false
		}

		if drawnSet.RedCubes > g.maxRedCubes {
			return false
		}

		if drawnSet.GreenCubes > g.maxGreenCubes {
			return false
		}
	}

	return true
}
